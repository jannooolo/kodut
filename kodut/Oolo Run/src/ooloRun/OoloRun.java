package ooloRun;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * OoloRun-i p�hiklass. p�hiklass millele rakenduvad lisad mis on m�ngule
 * vajalikud .
 *
 * 
 * @version 0.9a 18 Dec 2015
 * @author Janno Oolo
 * @since 1.8
 *
 */

public class OoloRun implements ActionListener, MouseListener, KeyListener {
	public static OoloRun oolorun;
	public final int laius = 900, k6rgus = 800;
	public Renderer renderer;
	public Rectangle pall;
	public int ticks, yTelg, skoor;
	public ArrayList<Rectangle> piilar;
	public Random rand;
	public boolean gameOver, started, topeltHype;
	public Timer timer;
	public StopWatch stopper;
	public static Pildid pildid;
	public static Heli heli;

	/**
	 * m�ngu main meetod.
	 * 
	 * @param oolorun
	 *            - loon objekti heli.
	 * @param pildid
	 *            - laeb pildid.
	 * @param heli.taust()
	 *            - k�ivitab taustaheli.
	 */
	public static void main(String[] args) {
		oolorun = new OoloRun();
		heli = new Heli();
		pildid = new Pildid();
		heli.taust();
	}

	/**
	 * konstruktor klass milles ehitatakse m�ng.
	 * 
	 * @param manguAken
	 *            - Jframega loodud aken.
	 * @param stopper
	 *            - luuakse stopper.
	 * @param renderer
	 *            - luuakse uus renderer mis hakkab kujundeid �lekirjutama.
	 * @param laius
	 *            - akna laius, fikseeritud 900.
	 * @param k6rgus
	 *            - akna k�rgus, fiskeeritud 800.
	 * @param pall
	 *            - kasutatakse mehikese joonistamisel tema antud kordinaate.
	 * @param piilar
	 *            - on massiv kus sees on piilarid ehk kollid.
	 */
	public OoloRun() {

		JFrame manguAken = new JFrame();
		timer = new Timer(20, this);
		stopper = new StopWatch();
		renderer = new Renderer();
		rand = new Random();
		manguAken.setTitle("OoloRun v0.3a");
		manguAken.add(renderer);
		manguAken.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		manguAken.setSize(laius, k6rgus);
		manguAken.addMouseListener(this);
		manguAken.addKeyListener(this);
		manguAken.setResizable(false);
		manguAken.setVisible(true);
		pall = new Rectangle(laius / 2 - 10, k6rgus - 140, 20, 20);
		piilar = new ArrayList<Rectangle>();

		addColumn(true);
		addColumn(true);
		addColumn(true);
		timer.start();
	}

	/**
	 * kollide ehk piilarite lisamine
	 * 
	 * @param suvalineArv
	 *            - Random arv nullist 300ni
	 * @param width
	 *            - kolli laius
	 * @param height
	 *            - kolli k�rgus
	 */
	public void addColumn(boolean start) {
		int width = 60;
		int suvalineArv = rand.nextInt(300);
		int height = 10 + suvalineArv;

		if (start) {
			piilar.add(new Rectangle(laius + width + piilar.size() * (500 + suvalineArv), k6rgus - height - 120, width,
					height));
		} else {
			piilar.add(new Rectangle(piilar.get(piilar.size() - 1).x + (500 + suvalineArv), k6rgus - height - 120,
					width, height));
		}
	}

	/**
	 * meetod mis tegeleb h�ppamise ja topelth�ppamisega.
	 * 
	 * @param started
	 *            - boolean v��rtus, n�itab kas m�ng k�ib.
	 * @param pall
	 *            - kuvab palli kordinaadid.
	 * @param yTelg
	 *            - n�itab k�rgust y telje suhtes
	 * @param topeltHype
	 *            - boolean v��rtus mis n�itab kas topelt h�pet saab teha v�i
	 *            mitte.
	 * @param gameOver
	 *            - boolean v��rtus mis n�itab kas m�ng on l�bi.
	 * @param skoor
	 *            - m�ngu skoor, alg v��rtus on null.
	 */
	public void hype() {

		if (!started) {
			started = true;
			stopper.start();
		} else if (!gameOver) {
			if (pall.y == k6rgus - 140) {
				if (yTelg > 0) {
					yTelg = 0;
					heli.piu();
				}
				yTelg -= 20;
				topeltHype = false;
			} else if (topeltHype == false) {
				if (yTelg > 0) {
					yTelg = 0;
				}
				yTelg -= 20;
				topeltHype = true;
				heli.piu();

			}
		}
		if (gameOver) {
			pall = new Rectangle(laius / 2 - 10, k6rgus - 140, 20, 20);
			piilar.clear();
			yTelg = 0;
			skoor = 0;

			addColumn(true);
			addColumn(true);
			addColumn(true);
			gameOver = false;
			stopper.start();
		}
	}

	/** meetodid mis kus toimub kollide liikumine. */
	@Override
	public void actionPerformed(ActionEvent e) {
		long kiiruseSuurenemine = stopper.getElapsedTimeSecs() / 4;
		long speed = 5 + kiiruseSuurenemine;

		ticks++;
		if (started) {

			for (int i = 0; i < piilar.size(); i++) {
				Rectangle column = piilar.get(i);
				column.x -= speed;
			}

			for (int i = 0; i < piilar.size(); i++) {

				Rectangle column = piilar.get(i);
				if (column.x + column.width < 0) {
					piilar.remove(column);
					addColumn(false);
				}
			}

			if (ticks % 2 == 0 && yTelg < 15) {
				yTelg += 2;
			}
			pall.y += yTelg;

			for (Rectangle column : piilar) {
				if (column.intersects(pall)) {
					if (!gameOver)
						stopper.stop();

					gameOver = true;

					pall.x = column.x - pall.width;
				}

				if (pall.y > k6rgus - 120 || pall.y < 0) {
					if (!gameOver)
						stopper.stop();
					gameOver = true;
				}
				if (pall.y + yTelg >= k6rgus - 120) {
					pall.y = k6rgus - 120 - pall.height;

				}
			}

		}

		renderer.repaint();
	}

	/**
	 * meetod kus joonistatakse kollid
	 * 
	 * @param column.height
	 *            - on kolli/piilari suurus.
	 */
	public void paintColumn(Graphics g, Rectangle column) {

		/** kollide joonistamine */
		if (column.height < 100) {
			g.drawImage(pildid.kollPilt, column.x, column.y, column.width, column.height, null);
		}
		if (column.height > 100 & column.height < 150) {
			g.drawImage(pildid.koll2Pilt, column.x, column.y, column.width, column.height, null);
		}
		if (column.height > 150 & column.height < 200) {
			g.drawImage(pildid.koll3Pilt, column.x, column.y, column.width, column.height, null);
		}
		if (column.height > 200 & column.height < 250) {
			g.drawImage(pildid.koll4Pilt, column.x, column.y, column.width, column.height, null);
		}
		if (column.height > 250) {
			g.drawImage(pildid.koll5Pilt, column.x, column.y, column.width, column.height, null);
		}
	}

	/**
	 * meetod kus joonistatakse kujundid
	 * 
	 * @param g.drawImage
	 *            - joonistab pildi.
	 * @param g.fillRect
	 *            - joonistab ristk�liku.
	 * @param g.setColor
	 *            - seab v�rvid
	 */
	public void repaint(Graphics g) {

		/** tausta joonistamine */
		g.drawImage(pildid.taustaPilt, 0, 0, 900, 800, null);

		g.setColor(Color.gray);
		g.fillRect(0, k6rgus - 120, laius, 120);

		g.setColor(Color.gray.brighter());
		g.fillRect(0, k6rgus - 120, laius, 20);

		/** mehikese joonistamine */
		g.drawImage(pildid.mehikenePilt, pall.x, pall.y, pall.width, pall.height, null);

		for (Rectangle column : piilar) {
			paintColumn(g, column);
		}
		g.setColor(Color.white);
		g.setFont(new Font("Arial", 1, 75));

		if (!started) {
			g.drawString("Alusta", 300, k6rgus / 2 - 50);
		}
		if (gameOver) {
			g.drawString("Koll sai su k�tte", 200, k6rgus / 2 - 50);
			g.drawString(String.valueOf(stopper.getElapsedTime() / 110 + "-ndal meetril"), 200, 500);

		}
		if (!gameOver && started) {

			g.drawString(String.valueOf(stopper.getElapsedTime() / 110 + "m"), laius / 2 - 25, 100);
		}

	}

	/** meetod mis n�itab mis juhtub kui vajutada hiirt */
	@Override
	public void mouseClicked(MouseEvent e) {
		hype();

	}

	/** autogenereeritud meetod */
	@Override
	public void mouseEntered(MouseEvent e) {

	}

	/** autogenereeritud meetod */
	@Override
	public void mouseExited(MouseEvent e) {

	}

	/** autogenereeritud meetod */
	@Override
	public void mousePressed(MouseEvent e) {

	}

	/** autogenereeritud meetod */
	@Override
	public void mouseReleased(MouseEvent e) {

	}

	/** autogenereeritud meetod */
	@Override
	public void keyPressed(KeyEvent e) {

	}

	/** meetod mis n�itab mis juhtub kui vajutada t�hikut */
	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			hype();
		}

	}

	/** autogenereeritud meetod */
	@Override
	public void keyTyped(KeyEvent e) {

	}

}