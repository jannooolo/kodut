package ooloRun;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

/**
 * OoloRun-i Heli klass. Siit saadakse m�ngule helid.
 * 
 * @version 0.9a 18 Dec 2015
 * @author Janno Oolo
 * @since 1.8
 * 
 */
public class Heli {

	/** konstruktor klass milles saadakse m�ngu heli. */
	public Heli() {

	}
	/**
	 * konstruktor klass milles saadakse m�ngu heli h�ppe jaoks.
	 * 
	 * @param jumpStream
	 *            - stream failist jump.wav
	 * @param clip.start
	 *            - k�ivitav heli.
	 * @param ex
	 *            - vea teade kui ei �nnesti heli m�ngida
	 */
	public void piu() {
		try {
			AudioInputStream jumpStream = AudioSystem.getAudioInputStream(new File("src/Helid/jump.wav"));
			Clip clip = AudioSystem.getClip();
			clip.open(jumpStream);
			clip.start();

		} catch (Exception ex) {
			System.out.println("viga heli m�ngimisel.");
			ex.printStackTrace();
		}
	}

	/**
	 * konstruktor klass milles saadakse m�ngu heli m�ngu l�pu jaoks.
	 * 
	 * @param loppStream
	 *            - stream failist lopp.wav
	 * @param clip.start
	 *            - k�ivitav heli.
	 * @param ex
	 *            - vea teade kui ei �nnesti heli m�ngida
	 */
	public void lopp() {
		try {
			AudioInputStream loppStream = AudioSystem.getAudioInputStream(new File("src/Helid/lopp.wav"));
			Clip clip = AudioSystem.getClip();
			clip.open(loppStream);
			clip.start();

		} catch (Exception ex) {
			System.out.println("viga heli m�ngimisel.");
			ex.printStackTrace();
		}
	}

	/**
	 * konstruktor klass milles saadakse m�ngu heli m�ngu tausta jaoks.
	 * 
	 * @param taustStream
	 *            - stream failist taust.wav
	 * @param clip.loop
	 *            - j�tab heli loopima
	 * @param ex
	 *            - vea teade kui ei �nnesti heli m�ngida
	 */
	public void taust() {
		try {
			AudioInputStream taustStream = AudioSystem.getAudioInputStream(new File("src/Helid/taust.wav"));
			Clip clip = AudioSystem.getClip();
			clip.open(taustStream);
			clip.loop(10);

		} catch (Exception ex) {
			System.out.println("viga heli m�ngimisel.");
			ex.printStackTrace();
		}
	}

}
