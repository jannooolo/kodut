package ooloRun;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
/**
 * OoloRun-i pildi klass.
 * Siit saadakse m�ngule pildid.
 * @version 0.9a 18 Dec 2015
 * @author 	Janno Oolo
 * @since 1.8
 * 
 */
public class Pildid {

    public static BufferedImage taustaPilt = null;
    public static BufferedImage mehikenePilt = null;
    public static BufferedImage kollPilt = null;
    public static BufferedImage koll2Pilt = null;
    public static BufferedImage koll3Pilt = null;
    public static BufferedImage koll4Pilt = null;
    public static BufferedImage koll5Pilt = null;
    /** konstruktor klass kus saadakse pildifailid 
     * @param taustaPilt - pilt mida kasutatakse taustana.
     * @param koll2Pilt - pilt mida kasutatakse k�ige v�iksema kollina.
     * @param koll2Pilt - pilt mida kasutatakse teisekollina.
     * @param koll3Pilt - pilt mida kasutatakse kolmanda kollina.
     * @param koll4Pilt - pilt mida kasutatakse neljanda kollina.
     * @param koll4Pilt - pilt mida kasutatakse k�ige suurma kollina.
     * @param mehikePilt - pilt mida kasutatakse mehikesena.
     * @param e - veateade kui ei leia pilti*/
    public Pildid(){
        try{
            taustaPilt = ImageIO.read(new File("src/pildid/taust.jpg"));
        }catch (Exception e){
            System.out.println("t�rge tausta laadimisel");
        }
        try{
            kollPilt = ImageIO.read(new File("src/pildid/koll.png"));
        }catch (Exception e){
            System.out.println("t�rge kolli laadimisel");
        }
        try{
            koll2Pilt = ImageIO.read(new File("src/pildid/koll2.png"));
        }catch (Exception e){
            System.out.println("t�rge koll2 laadimisel");
        }
        try{
            koll3Pilt = ImageIO.read(new File("src/pildid/koll3.png"));
        }catch (Exception e){
            System.out.println("t�rge koll3 laadimisel");
        }
        try{
            koll5Pilt = ImageIO.read(new File("src/pildid/koll5.png"));
        }catch (Exception e){
            System.out.println("t�rge koll5 laadimisel");
        }
        try{
            koll4Pilt = ImageIO.read(new File("src/pildid/koll4.png"));
        }catch (Exception e){
            System.out.println("t�rge koll4 laadimisel");
        }
        try{
        	mehikenePilt = ImageIO.read(new File("src/pildid/mees.png"));
        }catch (Exception e){
            System.out.println("t�rge mehe laadimisel");
        }
    }
  }

