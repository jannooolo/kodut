package ooloRun;

import java.awt.Graphics;
import javax.swing.JPanel;
/**
 * OoloRun-i �leprintimise klass.
 * Selle klassi abil priditakse kujundite uued asukohad �le.
 * @version 0.9a 18 Dec 2015
 * @author 	Janno Oolo
 * @since 1.8
 * 
 */
/** meetod mis prindib �le */
public class Renderer extends JPanel {

	private static final long serialVersionUID = 1L;

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		OoloRun.oolorun.repaint(g);

	}
}