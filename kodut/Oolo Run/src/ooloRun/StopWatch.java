package ooloRun;
/**
 * OoloRun-i stopperi klass.
 *
 * Selle ehitamisel on kasutatud interneti abi.
 * Selle klassi abil saab m�ng stopperi j�rgi aja.
 * @version 0.9a 18 Dec 2015
 * @author 	Janno Oolo
 * @since 1.8
 
 */
public class StopWatch {

	private long startTime = 0;
	private long stopTime = 0;
	private boolean running = false;
	 /** meetod mis alustab stopperi */
	public void start() {
		this.startTime = System.currentTimeMillis();
		this.running = true;
	}
	 /** meetod mis l�petab stopperi */
	public void stop() {
		this.stopTime = System.currentTimeMillis();
		this.running = false;
	}
	 /** meetod mis n�itab kulunud aega millisekundites long t��pi 
	  * @return elapsed - tagastab aja millisekundites*/
	public long getElapsedTime() {
		long elapsed;
		if (running) {
			elapsed = (System.currentTimeMillis() - startTime);
		} else {
			elapsed = (stopTime - startTime);
		}
		return elapsed;
	}
	 /** meetod mis n�itab kulunud aega sekundites long t��pi 
	  * @return elapsed - tagastab aja sekundites*/
	public long getElapsedTimeSecs() {
		long elapsed;
		if (running) {
			elapsed = ((System.currentTimeMillis() - startTime) / 1000);
		} else {
			elapsed = ((stopTime - startTime) / 1000);
		}
		return elapsed;
	}
}
