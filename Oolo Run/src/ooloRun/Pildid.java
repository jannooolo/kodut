package ooloRun;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class Pildid {

    public static BufferedImage taustaPilt = null;
    public static BufferedImage mehikenePilt = null;
    public static BufferedImage kollPilt = null;
    public static BufferedImage koll2Pilt = null;
    public static BufferedImage koll3Pilt = null;
    public static BufferedImage koll4Pilt = null;
    public static BufferedImage koll5Pilt = null;

    public Pildid(){
        try{
            taustaPilt = ImageIO.read(new File("src/pildid/taust.jpg"));
        }catch (Exception e){
            System.out.println("t�rge tausta laadimisel");
        }
        try{
            kollPilt = ImageIO.read(new File("src/pildid/koll.png"));
        }catch (Exception e){
            System.out.println("t�rge tausta laadimisel");
        }
        try{
            koll2Pilt = ImageIO.read(new File("src/pildid/koll2.png"));
        }catch (Exception e){
            System.out.println("t�rge tausta laadimisel");
        }
        try{
            koll3Pilt = ImageIO.read(new File("src/pildid/koll3.png"));
        }catch (Exception e){
            System.out.println("t�rge tausta laadimisel");
        }
        try{
            koll5Pilt = ImageIO.read(new File("src/pildid/koll5.png"));
        }catch (Exception e){
            System.out.println("t�rge mehe laadimisel");
        }
        try{
            koll4Pilt = ImageIO.read(new File("src/pildid/koll4.png"));
        }catch (Exception e){
            System.out.println("t�rge mehe laadimisel");
        }
        try{
        	mehikenePilt = ImageIO.read(new File("src/pildid/mees.png"));
        }catch (Exception e){
            System.out.println("t�rge mehe laadimisel");
        }
    }
  }

