package ooloRun;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

public class Heli {


	public Heli(){

	}

	public void piu() {
		try {
            AudioInputStream jumpStream = AudioSystem.getAudioInputStream(new File("src/Helid/jump.wav"));
			Clip clip = AudioSystem.getClip();
			clip.open(jumpStream);
			clip.start();

		} catch (Exception ex) {
			System.out.println("viga heli m�ngimisel.");
			ex.printStackTrace();
		}
	}

	public void lopp() {
		try {
            AudioInputStream loppStream = AudioSystem.getAudioInputStream(new File("src/Helid/lopp.wav"));
			Clip clip = AudioSystem.getClip();
			clip.open(loppStream);
			clip.start();

		} catch (Exception ex) {
			System.out.println("viga heli m�ngimisel.");
			ex.printStackTrace();
		}
	}

	public void taust() {
		try {
            AudioInputStream taustStream = AudioSystem.getAudioInputStream(new File("src/Helid/taust.wav"));
            Clip clip = AudioSystem.getClip();
			clip.open(taustStream);
			clip.loop(10);

		} catch (Exception ex) {
			System.out.println("viga heli m�ngimisel.");
			ex.printStackTrace();
		}
	}

}
