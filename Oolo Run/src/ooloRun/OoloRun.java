package ooloRun;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Random;


//p�hiklass millele rakenduvad lisad mis on m�ngule vajalikud   
public class OoloRun implements ActionListener, MouseListener, KeyListener {
	public static OoloRun oolorun;
	//l�plik suurus mis m�ngus ei muutu
	public final int laius = 900, k6rgus = 800;
	public Renderer renderer;
	public Rectangle pall;
	public int ticks, yTelg, skoor;
	public ArrayList<Rectangle> piilar;
	public Random rand;
	public boolean gameOver, started, topeltHype;
	public Timer timer;
	public StopWatch stop;

    public static Pildid pildid;
	public static Heli heli;
	
	

	//konstruktor kus ehitatakse m�ng �lesse
	public OoloRun() {
		
	
		//JFramega kuuakse uus aken "manguAken" millele antakse allpool paramameetrid
		JFrame manguAken = new JFrame();
		
		timer = new Timer(20, this);
		stop = new StopWatch();
		//timer ja stopper on vajalikud m�ngu skoori arvestamiseks ja kiiruseks
		renderer = new Renderer();
		//random nimega rand on kasutusel nii piilarite suuruse m��ramiseks kui ka piilarite vahe jaoks.
		rand = new Random();

		manguAken.setTitle("OoloRun v0.3a");
		manguAken.add(renderer);
		manguAken.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		manguAken.setSize(laius, k6rgus);
		manguAken.addMouseListener(this);
		manguAken.addKeyListener(this);
		//Et m�ngu akna suurust muuta ei saaks setResuzable(false)
		manguAken.setResizable(false);
		manguAken.setVisible(true);
		//pallisuurus ja piilarite massiv
		pall = new Rectangle(laius / 2 - 10, k6rgus - 140, 20, 20);
		piilar = new ArrayList<Rectangle>();
		

		addColumn(true);
		addColumn(true);
		addColumn(true);

		timer.start();
	}
	//piilarite lisamine
	public void addColumn(boolean start) {
		int width = 60;
		int suvalineArv = rand.nextInt(300);
		int height = 10 + suvalineArv;

		if (start) {
			piilar.add(new Rectangle(laius + width + piilar.size() * (500 + suvalineArv), k6rgus - height - 120,
					width, height));
		} else {
			piilar.add(new Rectangle(piilar.get(piilar.size() - 1).x + (500 + suvalineArv), k6rgus - height - 120,
					width, height));
		}

	}

		
	
	public void hype() {
	
		if (!started) {
			started = true;
			stop.start();
		} else if (!gameOver) {
			if (pall.y == k6rgus - 140) {
				if (yTelg > 0) {
					yTelg = 0;
					heli.piu();
				}
				yTelg -= 20;
				topeltHype = false;
			} else if (topeltHype == false) {
				if (yTelg > 0) {
					yTelg = 0;
				}
				yTelg -= 20;
				topeltHype = true;
				heli.piu();
				 
			}
		}
		if (gameOver) {
			pall = new Rectangle(laius / 2 - 10, k6rgus - 140, 20, 20);
			piilar.clear();
			yTelg = 0;
			skoor = 0;
			
			addColumn(true);
			addColumn(true);
			addColumn(true);
			gameOver = false;
			stop.start();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		long kiiruseSuurenemine = stop.getElapsedTimeSecs()/2;
		long speed = 5+kiiruseSuurenemine;

		ticks++;
		if (started) {

			for (int i = 0; i < piilar.size(); i++) {
				Rectangle column = piilar.get(i);
				column.x -= speed;
			}

			for (int i = 0; i < piilar.size(); i++) {

				Rectangle column = piilar.get(i);
				if (column.x + column.width < 0) {
					piilar.remove(column);
					addColumn(false);
				}
			}

			if (ticks % 2 == 0 && yTelg < 15) {
				yTelg += 2;
			}
			pall.y += yTelg;

			for (Rectangle column : piilar) {
				if (column.intersects(pall)) {
					if (!gameOver)
						stop.stop();
						
					gameOver = true;
					
					pall.x = column.x - pall.width;
				}
			
				if (pall.y > k6rgus - 120 || pall.y < 0) {
					if (!gameOver)
						stop.stop();
					gameOver = true;
				}
				if (pall.y + yTelg >= k6rgus - 120) {
					pall.y = k6rgus - 120 - pall.height;

				}
			}

		}
		
		renderer.repaint();
	}

	public void paintColumn(Graphics g, Rectangle column) {
		
		
		 if(column.height<100){
			 g.drawImage(pildid.kollPilt, column.x, column.y, column.width, column.height, null); 
		   }
		 if(column.height>100&column.height<150){
			   g.drawImage(pildid.koll2Pilt, column.x, column.y, column.width, column.height, null); 
		   }
		 if(column.height>150&column.height<200){
			   g.drawImage(pildid.koll3Pilt, column.x, column.y, column.width, column.height, null); 
		   }
		 if(column.height>200&column.height<250){
			   g.drawImage(pildid.koll4Pilt, column.x, column.y, column.width, column.height, null); 
		   }
		 if(column.height>250){
			   g.drawImage(pildid.koll5Pilt, column.x, column.y, column.width, column.height, null); 
		   }
	}
  
	public void repaint(Graphics g) {
		
		
        //taust:
        g.drawImage(pildid.taustaPilt,0,0, 900, 800, null);
		

		g.setColor(Color.gray);
		g.fillRect(0, k6rgus - 120, laius, 120);

		g.setColor(Color.gray.brighter());
		g.fillRect(0, k6rgus - 120, laius, 20);

		
        //mehike
        g.drawImage(pildid.mehikenePilt, pall.x, pall.y, pall.width, pall.height, null);

		for (Rectangle column : piilar) {
			paintColumn(g, column);
		}
		g.setColor(Color.white);
		g.setFont(new Font("Arial", 1, 100));

		if (!started) {
			g.drawString("Alusta", 250, k6rgus / 2 - 50);
		}
		if (gameOver) {
			g.drawString("l�pp", 200, k6rgus / 2 - 50);
			g.drawString(String.valueOf(stop.getElapsedTime()/110 + " meetrit"), laius / 2 - 225, 500);
			
		}
		if (!gameOver && started) {
		
			g.drawString(String.valueOf(stop.getElapsedTime()/110 + "m"), laius / 2 - 25, 100);
		}

	}

	public static void main(String[] args) {
		oolorun = new OoloRun();
        //loon objekti heli
		heli = new Heli();
        //laen pildid
        pildid = new Pildid();
        //k�ivitan taustaheli
		heli.taust();
		

	}


	@Override
	public void mouseClicked(MouseEvent e) {
		hype();

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			hype();
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}